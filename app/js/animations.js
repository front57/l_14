function showHeroElements() {
    let sleep = 250;
    const cb = (entries, observer) => {
        entries.forEach(entry => {
           if(entry.isIntersecting){
               setTimeout(() => {
                    entry.target.classList.add('normalize-element');
               }, sleep += 350);
           }
        });
    }
    const opts = {};
    const observer = new IntersectionObserver( cb, opts)
    const elements = document.querySelectorAll('.hero__img, .hero__title, .hero__desc, .hero__form');
    elements.forEach( e => {
        observer.observe(e);
    });

}
showHeroElements();

function showHeroUsers() {
    let sleep = 250;
    const cb = (entries, observer) => {
        entries.forEach(entry => {
            if(entry.isIntersecting){
                setTimeout(() => {
                    entry.target.classList.add('normalize-element');
                }, sleep += 150);
            }
        });
    }
    const opts = {};
    const observer = new IntersectionObserver( cb, opts)
    const elements = document.querySelectorAll('.users__img-wrapper, .users__desc');
    elements.forEach( e => {
        observer.observe(e);
    });
}
showHeroUsers()

function showCients() {
    let sleep = 250;
    const cb = (entries, observer) => {
        entries.forEach( entry => {
            if(entry.isIntersecting){
                setTimeout(() => {
                    entry.target.classList.add('normalize-element');
                }, sleep += 250);

            }
        });
    };
    const opts = {};
    const observer = new IntersectionObserver(cb, opts);
    const elements = document.querySelectorAll('.clients__item');
    elements.forEach( e => {
        observer.observe(e);
    })


}

showCients();


function showAccessTable() {
    let sleep = 150;
    let sleepTxt = 150;
    const cb = (entries, observer) => {
        entries.forEach(entry => {
            if(entry.isIntersecting){
                setTimeout( () => {
                    entry.target.classList.add('normalize-element');
                }, sleep += 250)
                setTimeout( () => {
                    entry.target.nextElementSibling.classList.add('normalize-element');
                },sleepTxt += 350);
            }
        })
    }

    const opts = {}
    const observer = new IntersectionObserver(cb, opts);
    const elements = document.querySelectorAll('.table__row-title');
    elements.forEach(e => {
        observer.observe(e);
    })
}

showAccessTable();


function showAccessElements() {
    let sleep = 450;
    const cb = (entries, observer) => {
        entries.forEach(entry => {
            if(entry.isIntersecting){
                setTimeout( () => {
                    entry.target.classList.add('normalize-element');
                }, sleep += 250)
            }
        })
    }

    const opts = {}
    const observer = new IntersectionObserver(cb, opts);
    const elements = document.querySelectorAll('.access__title, .access__link');
    elements.forEach(e => {
        observer.observe(e);
    })
}

showAccessElements();

function showPossibilitiesElements() {
    let sleep = 450;
    const cb = (entries, observer) => {
        entries.forEach(entry => {
            if(entry.isIntersecting){
                setTimeout( () => {
                    entry.target.classList.add('normalize-element');
                }, sleep += 250)
            }
        })
    }

    const opts = {}
    const observer = new IntersectionObserver(cb, opts);
    const elements = document.querySelectorAll('.possibilities__img-wrapper, .possibilities__subtitle, ' +
        '.possibilities__title, .possibilities__desc, .possibilities__link, .possibilities__img');
    elements.forEach(e => {
        observer.observe(e);
    })
}

showPossibilitiesElements();

function showRegister(){
    const cb = (entries, observer) => {
        entries.forEach(entry => {
            if(entry.isIntersecting){
                setTimeout( () => {
                    entry.target.classList.add('normalize-element');
                }, 250);
            }
        })
    }

    const opts = {}
    const observer = new IntersectionObserver(cb, opts);
    const elements = document.querySelectorAll('.register__row');
    elements.forEach(e => {
        observer.observe(e);
    })
}
showRegister();

function showBlogTitle() {
    const cb = (entries, observer) => {
        entries.forEach(entry => {
            if(entry.isIntersecting){
                setTimeout( () => {
                    entry.target.classList.add('normalize-element');
                }, 250);
            }
        })
    }

    const opts = {}
    const observer = new IntersectionObserver(cb, opts);
    const elements = document.querySelectorAll('.blog__title');
    elements.forEach(e => {
        observer.observe(e);
    })
}

showBlogTitle();

function showBlogCards() {
    let sleep = 250;
    const cb = (entries, observer) => {
        entries.forEach(entry => {
            if(entry.isIntersecting){
                setTimeout( () => {
                    entry.target.classList.add('normalize-element');
                }, sleep += 250);
            }
        })
    }

    const opts = {}
    const observer = new IntersectionObserver(cb, opts);
    const elements = document.querySelectorAll('.blog__card-link:not(.blog__card--big--big)');
    elements.forEach(e => {
        observer.observe(e);
    })
}

showBlogCards();

function showBlogFirstCard(){
    const cb = (entries, observer) => {
        entries.forEach(entry => {
            if(entry.isIntersecting){
                setTimeout( () => {
                    entry.target.classList.add('normalize-element');
                }, 250);
            }
        })
    }

    const opts = {}
    const observer = new IntersectionObserver(cb, opts);
    const element = document.querySelector('.blog__card-link.blog__card--big--big');
    observer.observe(element);
}

showBlogFirstCard();


function showRequestElements() {
    let sleep = 250;
    const cb = (entries, observer) => {
        entries.forEach(entry => {
            if(entry.isIntersecting){
                setTimeout( () => {
                    entry.target.classList.add('normalize-element');
                }, sleep +=250);
            }
        })
    }

    const opts = {}
    const observer = new IntersectionObserver(cb, opts);
    const elements = document.querySelectorAll('.request__title, .request__link-btn');
    elements.forEach( e => {
        observer.observe(e)
    })

}
showRequestElements();