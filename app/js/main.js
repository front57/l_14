

function menu(){
    let burger = document.querySelector('.hamburger');
    let header_menu = document.querySelector('.nav__menu');
    let body = document.querySelector('body');
    burger.addEventListener('click',  () => {
        burger.classList.toggle('active');
        header_menu.classList.toggle('active');
        body.classList.toggle('lock');
    });
}

menu();

function toggleAccordion(){
    const acc = document.getElementsByClassName("accordion__header");
    const panels = document.querySelectorAll('.accordion__body');

    [...acc].forEach((accordion, accIdx) => {
        accordion.addEventListener('click', function(){
            panels.forEach((panel, panelIdx) => {
                if(panelIdx !== accIdx){
                    panel.style.maxHeight = null;
                    panel.previousElementSibling.classList.remove('active');
                }else{
                    panel.style.maxHeight = panel.style.maxHeight ? null : panel.scrollHeight + "px";
                    panel.previousElementSibling.classList.toggle('active');
                }
            })
        });
    });
}

toggleAccordion();